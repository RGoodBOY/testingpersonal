import React from 'react';
import { Main, Dother } from './BarChart.styled';

const BarChart = ({ array }) => {
  return (
    <>
      <h2>Графики Pull New</h2>
      <Main>
        {array.map((el) => (
          <Dother height={el * 10} width="10" />
        ))}
      </Main>
    </>
  );
};

export default BarChart;
