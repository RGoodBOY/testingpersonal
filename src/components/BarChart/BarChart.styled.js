import styled from 'styled-components';

export const Main = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: flex-end;
`;
export const Dother = styled.div`
  background: red;
  width: 50px;
  height: ${({ height }) => `${height}px`};
`;
